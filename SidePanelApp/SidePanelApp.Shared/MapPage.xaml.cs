﻿using System;
using Xamarin.Forms;

namespace SidePanelApp
{
	public partial class MapPage : ContentPage
	{
	    private bool _panelIsExpanded;
	    private double _panelWidth;

        //This implementation uses this.Width to set the columns' widths based on the actual "size" of the current page. 

        public MapPage()
        {
            InitializeComponent();
            SizeChanged += SetSizes;
            _panelIsExpanded = false;
        }

        private void SetSizes(object sender, EventArgs e)
        {
            if (this.Width > 0)
            {
                _panelWidth = this.Width * .3;
                SideBar.Width = this.Width * .05;
                ButtonRow.Height = this.Width * .05;
                SizeChanged -= SetSizes;
            }
        }

        public void OnPanelButtonClicked(object sender, EventArgs args)
	    {
            Animation panelAnimation;
            if (_panelIsExpanded)
            {
                panelAnimation = new Animation(callback: v => PanelColumn.Width = v, start: _panelWidth, end: 0);
                _panelIsExpanded = false;
            }
            else
            {
                panelAnimation = new Animation(callback: v => PanelColumn.Width = v, start: 0, end: _panelWidth);
                _panelIsExpanded = true;
            }

            panelAnimation.Commit(this, "paneAnimation", 16U, 500U, Easing.CubicInOut);
        }

        //**************************************************************************************
        //the following implementation hard codes the column sizes, based on what type of device is used - desktop/phone/tablet

        //public MapPage()
        //{
        //    InitializeComponent();
        //    SetSizes();
        //    _panelIsExpanded = false;
        //}

        //private void SetSizes()
        //{
        //    var idiom = Device.Idiom;
        //    if (idiom == TargetIdiom.Desktop)
        //    {
        //        SideBar.Width = 50;
        //        _panelWidth = 300;
        //    }
        //    else if (idiom == TargetIdiom.Phone || idiom == TargetIdiom.Tablet)
        //    {
        //        SideBar.Width = 30;
        //        ButtonRow.Height = 30;
        //        _panelWidth = 150;
        //    }
        //}

        //public void OnPanelButtonClicked(object sender, EventArgs args)
        //{
        //    var thisWidth = this.Width;
        //    Animation panelAnimation;
        //    if (_panelIsExpanded)
        //    {
        //        panelAnimation = new Animation(callback: v => PanelColumn.Width = v, start: _panelWidth, end: 0);
        //        _panelIsExpanded = false;
        //    }
        //    else
        //    {
        //        panelAnimation = new Animation(callback: v => PanelColumn.Width = v, start: 0, end: _panelWidth);
        //        _panelIsExpanded = true;
        //    }

        //    panelAnimation.Commit(this, "paneAnimation", 16U, 500U, Easing.CubicInOut);
        //}
        //**************************************************************************************

        // Map initialization logic is contained in MapViewModel.cs
    }
}
